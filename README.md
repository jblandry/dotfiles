# dotfiles

## Add spaces in Dock

```shell
defaults write com.apple.dock persistent-apps -array-add '{"tile-type"="spacer-tile";}'; killall Dock
```
## GPG

```shell
brew install gnupg
cd /opt/homebrew/bin; ln -s gpg gpg2

gpg --allow-secret-key-import --import <SECRET_KEY_FILE>
gpg --edit-key <KEY_ID>

<trust>
```
SourceTree : /User/jblandry/dotfiles/bin

## Symlinks

```shell
. ~/dotfiles/setup.sh
```


## Beyond Compare
~/Library/Application Support/Beyond Compare -> local/app/beyond-compare


## Optimize video
ffmpeg -i source.mp4  -vcodec libx264 -crf 28 output.mp4
