#!/bin/bash

### [Cask](http://caskroom.io/)
brew install caskroom/cask/brew-cask


brew cask install flux
brew cask install lastpass
brew cask install magicprefs
brew cask install unrarx

brew cask install brave
brew cask install firefox
brew cask install google-chrome

brew cask install atom
brew cask install iterm2
brew cask install path-finder
brew cask install sequel-pro
brew cask install sourcetree

brew cask install slack
brew cask install spotify
brew cask install vlc
brew cask install vox
brew cask install vox-preferences-pane

brew cask install qlcolorcode
brew cask install qlstephen
brew cask install qlmarkdown
brew cask install quicklook-json
brew cask install quicklook-csv
brew cask install qlimagesize


# Perso
brew cask install airtame
brew cask install google-backup-and-sync
brew cask install mixxx
brew cask install steam


# Work
brew cask install adobe-illustrator-cc
brew cask install adobe-photoshop-cc
brew cask install beyond-compare
brew cask install filezilla
brew cask install fluid
brew cask install joinme
brew cask install wmail


# Chrome Canary
