# Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

	# Intel - x86
	if [ -e /usr/local/Homebrew/bin/brew ]; then
			homebrew_binary="/usr/local/Homebrew/bin/brew"

	# Apple Silicon - ARM
	else
			homebrew_binary="/opt/homebrew/bin/brew"
	fi

	# Shamelessly copied from https://github.com/Homebrew/install/blob/70722cb5a3b689d87b1869c6d380f559772e8b1b/install.sh
	case "${SHELL}" in
	*/bash*)
			if [[ -r "${HOME}/.bash_profile" ]]
			then
			shell_profile="${HOME}/.bash_profile"
			else
			shell_profile="${HOME}/.profile"
			fi
			;;
	*/zsh*)
			shell_profile="${HOME}/.zprofile"
			;;
	*)
			shell_profile="${HOME}/.profile"
			;;
	esac

	# Load Homebrew environment variables into current session
	echo 'eval "$(${homebrew_binary} shellenv)"' >> ${shell_profile}
	eval $(${homebrew_binary} shellenv)

	# Formulae
	brew tap homebrew/cask
	brew tap homebrew/core

	brew install bash
	brew install bash-completion
	brew install git
	brew install nano
	brew install node

	# Cleanup
	brew cleanup --verbose
	brew doctor || true


# Mine
sudo chown -R $USER:staff /usr/local/* || true


# Node.js
npm install npm --location=global --no-audit --force
npm install concurrently --location=global

# Force Bash
sudo sh -c "echo $(brew --prefix)/bin/bash >> /etc/shells"
chsh -s $(brew --prefix)/bin/bash


# Symlink shell
rm -f ~/.bash_profile
ln -s dotfiles/shell/bash/bash_profile ~/.bash_profile

rm -f ~/.zshrc
ln -s dotfiles/shell/zsh/zshrc ~/.zshrc

rm -f ~/.nanorc
ln -s dotfiles/shell/nanorc ~/.nanorc

rm -f ~/.vimrc
ln -s dotfiles/shell/vimrc ~/.vimrc


# Symlink Git
rm -f ~/.gitconfig
ln -s dotfiles/git/gitconfig ~/.gitconfig


# Symlink local stuff
rm -f ~/.ssh
ln -s dotfiles-local/ssh ~/.ssh

rm -f ~/.npmrc
ln -s dotfiles-local/npmrc ~/.npmrc
